package time;

/**
 * 
 * @author Brian Hadskis
 * TimeTest JUnit test class for the Time class
 * (Time class written by Wendi Jollymore, modified by Liz Dancy)
 * 
 */

import static org.junit.Assert.*;

import org.junit.Test;


public class TimeTest {

	@Test
	public void testGetTotalSeconds_Regular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	}
	
	@Test(expected=NumberFormatException.class)
	public void testGetTotalSeconds_Exceptional() {
		int totalSeconds = Time.getTotalSeconds("01:01:0a");
		fail("No error thrown after passing non-numerical character.");
	}

	@Test
	public void testGetTotalSeconds_BoundaryIn() {
		int totalSeconds = Time.getTotalSeconds("99:59:59");
		assertTrue("The time provided does not match the result", totalSeconds == 359999);
	}
	
	@Test(expected=NumberFormatException.class)
	public void testGetTotalSeconds_BoundaryOut() {
		int totalSeconds = Time.getTotalSeconds("100:00:00");
		fail("Time provided was too long, NumberFormatException was expected.");
	}
	
	@Test
	public void testGetMilliseconds_Regular() {
		int milliseconds = Time.getMilliseconds("12:05:05:05");
		assertTrue("The time provided does not match the result", milliseconds == 5);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetMilliseconds_Exceptional() {
		int milliseconds = Time.getMilliseconds("12:05:50:0A");
		fail("Invalid milliseconds");
	}
	
	@Test
	public void testGetMilliseconds_BoundaryIn() {
		int milliseconds = Time.getMilliseconds("12:05:05:999");
		assertTrue("Invalid entry", milliseconds == 999);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetMilliseconds_BoundaryOut() {
		int milliseconds = Time.getMilliseconds("12:05:05:1000");
		fail("Invalid number of milliseconds");
	}

}
